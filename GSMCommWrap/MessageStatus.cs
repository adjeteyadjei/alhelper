﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ALGSMCommWrap
{
    public enum MessageStatus
    {
        ALL,
        READ,
        UNREAD,
        SENT,
        UNSENT
    }
}
