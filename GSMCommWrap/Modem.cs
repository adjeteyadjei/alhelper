﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ALGSMCommWrap
{
    public class Modem
    {
        public string SerialNumber { get; set; }
        public string Name { get; set; }
        public string Port { get; set; }
    }
}
