﻿namespace ALGSMCommWrap
{
    public class MessageType
    {
        public static string Inbox = "Inbox";
        public static string Sent = "Sent";
        public static string Draft = "Draft";
        public static string Outbox = "Outbox";
    }
}
