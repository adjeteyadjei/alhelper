﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ALGSMCommWrap
{
    public class MessageLog
    {
        public string PhoneNumber { get; set; }
        public string Text { get; set; }
        public DateTime TimeStamp { get; set; }
        public int Index { get; set; }
        public string Type { get; set; }
    }
}
