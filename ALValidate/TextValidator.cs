﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace ALValidate
{
    public class TextValidator
    {
        private static Regex _reg;
        private static string _expr = string.Empty;

        public static bool IsValidEmail(string email)
        {
            _expr = @"(^([a-zA-Z0-9_\-])([a-zA-Z0-9_\-\.]*)@(\[((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.){3}|" +
            @"((([a-zA-Z0-9\-]+)\.)+))([a-zA-Z]{2,}|(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\])$)|(^ *$)";

             _reg = new Regex(_expr);
            return _reg.IsMatch(email);
        }

        public static bool IsValidIp(string ip)
        {
            _expr = @"(^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$)|(^ *$)";
            _reg = new Regex(_expr);
            
            if (!_reg.IsMatch(ip)) return false;

            var parts = ip.Split('.');

            if (Int32.Parse(parts[0]) == 0) return false;
            return Int32.Parse(parts[3]) != 0 && parts.All(t => Int32.Parse(t) <= 255);
        }

        public static bool IsNumber(string number)
        {
            _expr = @"(^\d{0,9}$)|(^ *$)";
            _reg = new Regex(_expr);

            return _reg.IsMatch(number);
        }

        public static bool IsCurrency(string number)
        {
            _expr = @"^\d{0,9}(\.\d{0,9})?$";
            _reg = new Regex(_expr);

            return _reg.IsMatch(number);
        }

        public static bool IsPhoneNumber(string number)
        {
            _expr = @"(^(\+?\-? *[0-9]+)([0-9 ]*)([0-9 ])*$)|(^ *$)";
            _reg = new Regex(_expr);

            return _reg.IsMatch(number);
        }
    }
}
