﻿using System.Collections.Generic;
using System.Linq;

namespace ALValidate
{
    public class Validator
    {
        public string ErrorSummary = string.Empty;
        private bool _valid = true;
        public bool Validate(IEnumerable<PropToVal> propToVals)
        {
            ErrorSummary = string.Empty;
            _valid = true;
            foreach (var prop in propToVals)
            {
                switch (prop.ValidationType)
                {
                    case ValidationType.Required:
                        if (string.IsNullOrEmpty(prop.Value))
                        {
                            _valid = false;
                            ErrorSummary += prop.Name + " is required.\n";
                        }
                        break;
                    case ValidationType.Number:
                        if (!TextValidator.IsNumber(prop.Value))
                        {
                            _valid = false;
                            ErrorSummary += prop.Name + " is not a valid.\n";
                        }
                        break;
                    case ValidationType.PhoneNumber:
                        if (!TextValidator.IsPhoneNumber(prop.Value))
                        {
                            _valid = false;
                            ErrorSummary += prop.Name + " is not a valid.\n";
                        }
                        break;
                    case ValidationType.Currency:
                        if (!TextValidator.IsCurrency(prop.Value))
                        {
                            _valid = false;
                            ErrorSummary += prop.Name + " is not a valid.\n";
                        }
                        break;
                    case ValidationType.Email:
                        if (!TextValidator.IsValidEmail(prop.Value))
                        {
                            _valid = false;
                            ErrorSummary += prop.Name + " is not a valid.\n";
                        }
                        break;
                    case ValidationType.Options:
                        if (!prop.Options.Contains(prop.Value))
                        {
                            _valid = false;
                            ErrorSummary += prop.Name + " is not a valid.\n";
                        }
                        break;
                }
            }

            return _valid;
        }
    }

    public class PropToVal
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public ValidationType ValidationType { get; set; }
        public string[] Options { get; set; }
    }

    public enum ValidationType
    {
        Required,
        Email,
        Number,
        Currency,
        PhoneNumber,
        Options
    }
}
