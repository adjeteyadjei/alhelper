﻿namespace ALHelpers.Web
{
    public class ResultObj
    {
        public int total { get; set; }
        public object data { get; set; }
        public string message { get; set; }
        public bool success { get; set; }

    }
}
