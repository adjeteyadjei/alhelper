﻿using System;

namespace ALHelpers.Web
{
    public class WebHelper
    {
        /// <summary>
        /// Builds the result object.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="msg">The MSG.</param>
        /// <param name="success">if set to <c>true</c> [success].</param>
        /// <param name="total">The total.</param>
        /// <returns></returns>
        public static ResultObj BuildResult(object data, string msg, bool success, int total)
        {
            var results = new ResultObj
            {
                data = data,
                message = msg,
                success = success,
                total = total
            };
            return results;
        }

        /// <summary>
        /// Build exceptions object.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <returns></returns>
        public static ResultObj Exception(Exception ex)
        {
            return BuildResult(null, ExceptionProcessor.ErrorMessage(ex), false, 0);
        }
    }
}
