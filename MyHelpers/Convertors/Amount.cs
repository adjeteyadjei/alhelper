﻿using System.Linq;

namespace ALHelpers.Convertors
{
    public class Amount
    {
        private static string _majorc;
        private static string _minorc;

        /// <summary>
        /// Initializes a new instance of the <see cref="Amount"/> class.
        /// </summary>
        /// <param name="majorCurrency">The major currency.</param>
        /// <param name="minorCurrency">The minor currency.</param>
        private static void SetCurrency(string majorCurrency, string minorCurrency)
        {
            _majorc = " " + majorCurrency;
            _minorc = " " + minorCurrency;
        }

        /// <summary>
        /// To the english.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="majorCurrency"></param>
        /// <param name="minorCurrency"></param>
        /// <returns></returns>
        public static string ToEnglish(double amount, string majorCurrency= "", string minorCurrency="")
        {
            SetCurrency(majorCurrency, minorCurrency);
            if (amount < 1) return string.Empty;
            var amt = amount.ToString("#,##.00");
            var results =  ChangeAmount(amt.StartsWith("-") ? amt.Substring(1) : amt);
            results = results.Replace("and " + majorCurrency, majorCurrency);
            return results;
        }

        /// <summary>
        /// To the english.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="majorCurrency"></param>
        /// <param name="minorCurrency"></param>
        /// <returns></returns>
        public static string ToEnglish(float amount, string majorCurrency = "", string minorCurrency = "")
        {
            SetCurrency(majorCurrency, minorCurrency);
            if (amount < 1) return string.Empty;
            var amt = amount.ToString("#,##.00");
            var results = ChangeAmount(amt.StartsWith("-") ? amt.Substring(1) : amt);
            results = results.Replace("and " + majorCurrency, majorCurrency);
            return results;
        }

        /// <summary>
        /// To the english.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="majorCurrency"></param>
        /// <param name="minorCurrency"></param>
        /// <returns></returns>
        public static string ToEnglish(int amount, string majorCurrency = "", string minorCurrency = "")
        {
            SetCurrency(majorCurrency, minorCurrency);
            if (amount < 1) return string.Empty;
            var amt = amount.ToString("#,##.00");
            var results = ChangeAmount(amt.StartsWith("-") ? amt.Substring(1) : amt);
            results = results.Replace("and " + majorCurrency, majorCurrency);
            return results;
        }

        /// <summary>
        /// Changes the amount.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <returns></returns>
        private static string ChangeAmount(string amount)
        {
            var wholeNumber = amount.Substring(0, amount.IndexOf('.'));
            var decimalNumber = amount.Substring(amount.IndexOf('.') + 1);

            var mWord = GetMainWord(wholeNumber);
            if (mWord != string.Empty) mWord = mWord.Trim() + _majorc;

            var pWord = Tens(decimalNumber);
            if (pWord.Trim() != string.Empty) pWord = pWord.Trim() + _minorc;

            var amtInWord = mWord + " " + pWord;

            return amtInWord.Trim() + " Only.";
        }

        /// <summary>
        /// Gets the main word.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns></returns>
        private static string GetMainWord(string number)
        {
            string words;
            var commaCount = number.Split(',').Count() - 1;
            switch (commaCount)
            {
                case 1:
                    //Thousand
                    words = Thousands(number);
                    break;
                case 2:
                    //Million
                    words = Millions(number);
                    break;
                case 3:
                    //Billion
                    words = Billions(number);
                    break;
                case 4:
                    //Trillion
                    words = Trillions(number);
                    break;
                default:
                    words = Hundreds(number, false);
                    break;
            }
            return words;
        }

        /// <summary>
        /// Oneses the specified digits.
        /// </summary>
        /// <param name="digits">The digits.</param>
        /// <returns></returns>
        private static string Ones(string digits)
        {
            var digit = int.Parse(digits);
            var name = "";
            switch (digit)
            {
                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }

        /// <summary>
        /// Tenses the specified digits.
        /// </summary>
        /// <param name="digits">The digits.</param>
        /// <returns></returns>
        private static string Tens(string digits)
        {
            var digit = int.Parse(digits);
            if (digit < 10) return Ones(digits);
            string name = null;
            switch (digit)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (digit > 0)
                    {
                        name = Tens(digits.Substring(0, 1) + "0") + " " + Ones(digits.Substring(1));
                    }
                    break;
            }
            return name;
        }

        /// <summary>
        /// Hundredses the specified digits.
        /// </summary>
        /// <param name="digits">The digits.</param>
        /// <param name="endstr">if set to <c>true</c> [endstr].</param>
        /// <returns></returns>
        private static string Hundreds(string digits, bool endstr)
        {
            var digit = int.Parse(digits);
            if (digit < 100)
            {
                if (endstr) return "and " + Tens(digits);
                return Tens(digits);
            }
            string name = null;
            switch (digit)
            {
                case 100:
                    name = "One Hundred";
                    break;
                case 200:
                    name = "Two Hundred";
                    break;
                case 300:
                    name = "Three Hundred";
                    break;
                case 400:
                    name = "Four Hundred";
                    break;
                case 500:
                    name = "Five Hundred";
                    break;
                case 600:
                    name = "Six Hundred";
                    break;
                case 700:
                    name = "Seven Hundred";
                    break;
                case 800:
                    name = "Eight Hundred";
                    break;
                case 900:
                    name = "Nine Hundred";
                    break;
                default:
                    if (digit > 0)
                    {
                        name = Hundreds(digits.Substring(0, 1) + "00", false) + " and " + Tens(digits.Substring(1));
                    }
                    break;
            }
            return name;
        }

        /// <summary>
        /// Thousandses the specified digits.
        /// </summary>
        /// <param name="digits">The digits.</param>
        /// <returns></returns>
        private static string Thousands(string digits)
        {
            var fword = Hundreds(GetFirstDigits(digits), false);
            if (fword.Trim() != "") fword = fword + " Thousand ";
            return fword + Hundreds(GetReminderDigits(digits), true);
        }

        /// <summary>
        /// Millionses the specified digits.
        /// </summary>
        /// <param name="digits">The digits.</param>
        /// <returns></returns>
        private static string Millions(string digits)
        {
            var fword = Hundreds(GetFirstDigits(digits), false);
            if (fword.Trim() != "") fword = fword + " Million ";
            return fword + Thousands(GetReminderDigits(digits));
        }

        /// <summary>
        /// Billionses the specified digits.
        /// </summary>
        /// <param name="digits">The digits.</param>
        /// <returns></returns>
        private static string Billions(string digits)
        {
            var fword = Hundreds(GetFirstDigits(digits), false);
            if (fword.Trim() != "") fword = fword + " Billion ";
            return fword + Millions(GetReminderDigits(digits));
        }

        /// <summary>
        /// Trillionses the specified digits.
        /// </summary>
        /// <param name="digits">The digits.</param>
        /// <returns></returns>
        private static string Trillions(string digits)
        {
            return Hundreds(GetFirstDigits(digits), false) + " Trillion " + Billions(GetReminderDigits(digits));
        }

        /// <summary>
        /// Gets the first digits.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns></returns>
        private static string GetFirstDigits(string number)
        {
            return number.Substring(0, number.IndexOf(','));
        }

        /// <summary>
        /// Gets the reminder digits.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns></returns>
        private static string GetReminderDigits(string number)
        {
            return number.Substring(number.IndexOf(',') + 1);
        }

    }
}
