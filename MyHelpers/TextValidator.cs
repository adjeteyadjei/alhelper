﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace ALHelpers
{
    public class TextValidator
    {
        private static Regex _reg;
        private static string _expr = string.Empty;

        /// <summary>
        /// Determines whether [is valid email] [the specified email].
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns></returns>
        public static bool IsValidEmail(string email)
        {
            _expr = @"(^([a-zA-Z0-9_\-])([a-zA-Z0-9_\-\.]*)@(\[((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.){3}|" +
            @"((([a-zA-Z0-9\-]+)\.)+))([a-zA-Z]{2,}|(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\])$)|(^ *$)";

             _reg = new Regex(_expr);
            return _reg.IsMatch(email);
        }

        /// <summary>
        /// Determines whether [is valid ip] [the specified ip].
        /// </summary>
        /// <param name="ip">The ip.</param>
        /// <returns></returns>
        public static bool IsValidIp(string ip)
        {
            _expr = @"(^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$)|(^ *$)";
            _reg = new Regex(_expr);
            
            if (!_reg.IsMatch(ip)) return false;

            var parts = ip.Split('.');

            if (Int32.Parse(parts[0]) == 0) return false;
            return Int32.Parse(parts[3]) != 0 && parts.All(t => Int32.Parse(t) <= 255);
        }

        /// <summary>
        /// Determines whether the specified number is number.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns></returns>
        public static bool IsNumber(string number)
        {
            _expr = @"(^\d{0,9}$)|(^ *$)";
            _reg = new Regex(_expr);

            return _reg.IsMatch(number);
        }

        /// <summary>
        /// Determines whether the specified number is currency.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns></returns>
        public static bool IsCurrency(string number)
        {
            _expr = @"^\d{0,9}(\.\d{0,9})?$";
            _reg = new Regex(_expr);

            return _reg.IsMatch(number);
        }

        /// <summary>
        /// Determines whether [is phone number] [the specified number].
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns></returns>
        public static bool IsPhoneNumber(string number)
        {
            _expr = @"(^(\+?\-? *[0-9]+)([0-9 ]*)([0-9 ])*$)|(^ *$)";
            _reg = new Regex(_expr);

            return _reg.IsMatch(number);
        }
    }
}
