﻿using System;
using System.Windows.Forms;

namespace ALHelpers.WinForm
{
    public class FormHelper
    {
        /// <summary>
        /// Clears the controls.
        /// </summary>
        /// <param name="controls">The controls.</param>
        public static void ClearControls(Control.ControlCollection controls)
        {
            foreach (Control ctrl in controls)
            {
                if (ctrl.GetType() == typeof(TextBox)) ctrl.Text = string.Empty;
                if (ctrl.GetType() == typeof(RichTextBox)) ctrl.Text = string.Empty;
                if (ctrl.GetType() == typeof(ComboBox))
                {
                    var cb = (ComboBox)ctrl;
                    cb.SelectedIndex = -1;
                }
                if (ctrl.GetType() == typeof(ListBox))
                {
                    var lst = (ListBox)ctrl;
                    lst.SelectedIndex = -1;
                }
                if (ctrl.GetType() == typeof(CheckBox))
                {
                    var ck = (CheckBox)ctrl;
                    ck.Checked = false;
                }
                if (ctrl.GetType() == typeof(RadioButton))
                {
                    var rb = (RadioButton)ctrl;
                    rb.Checked = false;
                }
                if (ctrl.GetType() == typeof(DateTimePicker))
                {
                    var dtp = (DateTimePicker)ctrl;
                    dtp.Value = DateTime.Today.Date;
                }
                if (ctrl.GetType() == typeof(DataGridView))
                {
                    var dgv = (DataGridView)ctrl;
                    dgv.Rows.Clear();
                }
                if (ctrl.GetType() == typeof(PictureBox))
                {
                    var picBox = (PictureBox)ctrl;
                    picBox.Image = null;
                    picBox.ImageLocation = null;
                }
                if (ctrl.GetType() == typeof(CheckedListBox))
                {
                    var chl = (CheckedListBox)ctrl;
                    for (var i = 0; i < chl.Items.Count; i++)
                    {
                        chl.SetItemChecked(i, false);
                    }
                }

                if (ctrl.GetType() == typeof(GroupBox))
                {
                    var gb = (GroupBox)ctrl;
                    ClearControls(gb.Controls);
                }

                if (ctrl.GetType() == typeof(Panel))
                {
                    var pl = (Panel)ctrl;
                    ClearControls(pl.Controls);
                }

                if (ctrl.GetType() != typeof (TabControl)) continue;
                var tc = (TabControl)ctrl;
                foreach (Control tab in tc.Controls)
                {
                    ClearControls(tab.Controls);
                }
            }
        }

        /// <summary>
        /// Clears the controls.
        /// </summary>
        /// <param name="form">The form.</param>
        public static void ClearControls(Form form)
        {
            ClearControls(form.Controls);
        }
    }
}
