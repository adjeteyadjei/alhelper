﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ALHelpers.WinForm
{
    public class FormValidate
    {
        private Color _errorColor = Color.NavajoWhite;

        private string _errorSummary = string.Empty;

        /// <summary>
        /// Sets the color of the error dor.
        /// </summary>
        /// <param name="color">The color.</param>
        public void SetErrorColor(Color color)
        {
            _errorColor = color;
        }

        private bool _valid = true;

        /// <summary>
        /// Determines whether [is valid controls] [the specified controls to validate].
        /// </summary>
        /// <param name="controlsToValidate">The controls to validate.</param>
        /// <returns></returns>
        public bool IsValidControls(List<ControlToValidate> controlsToValidate)
        {
            controlsToValidate.ForEach(q => q.Control.BackColor = Color.White);

            _valid = true; _errorSummary = string.Empty;
            foreach (var item in controlsToValidate)
            {
                switch (item.ValidateType)
                {
                    case ValidationType.Required:
                        if (!HasValue(item.Control))
                        {
                            _valid = false;
                            if (!string.IsNullOrEmpty(item.ErrorMessage)) _errorSummary += item.ErrorMessage + ".\n";
                        }
                        break;
                    case ValidationType.Checked:
                        if (item.Control.GetType() == typeof(CheckedListBox))
                        {
                            var chl = (CheckedListBox)item.Control;
                            if (chl.CheckedItems.Count < 1) SetValidate(item);
                        }
                        break;
                    case ValidationType.Email:
                        if (!TextValidator.IsValidEmail(item.Control.Text)) SetValidate(item);
                        break;
                    case ValidationType.Number:
                        if (!TextValidator.IsNumber(item.Control.Text)) SetValidate(item);
                        break;
                    case ValidationType.Currency:
                        if (!TextValidator.IsCurrency(item.Control.Text)) SetValidate(item);
                        break;
                    case ValidationType.PhoneNumber:
                        if (!TextValidator.IsPhoneNumber(item.Control.Text)) SetValidate(item);
                        break;
                }
            }

            return _valid;
        }

        /// <summary>
        /// Sets the validate.
        /// </summary>
        /// <param name="ctrl">The control.</param>
        private void SetValidate(ControlToValidate ctrl)
        {
            ctrl.Control.BackColor = _errorColor;
            _valid = false;
            if (!string.IsNullOrEmpty(ctrl.ErrorMessage)) _errorSummary += ctrl.ErrorMessage + ".\n";
        }

        /// <summary>
        /// Determines whether the specified control has value.
        /// </summary>
        /// <param name="ctrl">The control.</param>
        /// <returns></returns>
        private bool HasValue(Control ctrl)
        {
            if (!string.IsNullOrEmpty(ctrl.Text.Trim())) return true;
            ctrl.BackColor = _errorColor;
            return false;
        }

        /// <summary>
        /// Validations the summary.
        /// </summary>
        /// <returns></returns>
        public string ValidationSummary()
        {
            return _errorSummary.Trim();
        }

    }

    public class ControlToValidate
    {
        public Control Control { get; set; }
        public ValidationType ValidateType { get; set; }
        public string ErrorMessage { get; set; }
    }

    public enum ValidationType
    {
        Required,
        Email,
        Number,
        Currency,
        PhoneNumber,
        Checked,
        Selected
    }
}
