﻿using System.IO;
using System.Linq;

namespace ALHelpers
{
    public class Image
    {
        /// <summary>
        /// Convert image to the binary.
        /// </summary>
        /// <param name="imagePath">The image path.</param>
        /// <returns></returns>
        public static byte[] ToBinary(string imagePath)
        {
            if (string.IsNullOrEmpty(imagePath)) return null;
            var fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
            var buffer = new byte[fileStream.Length];
            fileStream.Read(buffer, 0, (int)fileStream.Length);
            fileStream.Close();
            return buffer;
        }

        /// <summary>
        /// Convert image to the binary.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns></returns>
        public static byte[] ToBinary(System.Drawing.Image image)
        {
            byte[] data;
            using(var ms = new MemoryStream())
            {
                image.Save(ms, image.RawFormat);
                data = ms.ToArray();
            }
            return data;
        }

        /// <summary>
        /// Convert binary to image.
        /// </summary>
        /// <param name="binaryData">The binary data.</param>
        /// <returns></returns>
        public static System.Drawing.Image BinaryToImage(byte[] binaryData)
        {
            if (binaryData == null) return null;

            var buffer = binaryData.ToArray();
            var memStream = new MemoryStream();
            memStream.Write(buffer, 0, buffer.Length);
            return System.Drawing.Image.FromStream(memStream);
        }

        /// <summary>
        /// Saves the image.
        /// </summary>
        /// <param name="saveLocation">The save location.</param>
        /// <param name="image">The image.</param>
        public static void SaveImage(string saveLocation, System.Drawing.Image image)
        {
            image.Save(saveLocation);
        }
        
    }
}
