﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;

namespace ALHelpers
{
    public class String
    {
        static readonly CultureInfo CultureInfo = Thread.CurrentThread.CurrentCulture;
        static readonly TextInfo TextInfo = CultureInfo.TextInfo;

        /// <summary>
        /// Formats the specified string.
        /// Using magic string with property name instead of numbers
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public static string Format(string str, object obj)
        {
            var properties = new List<string>();

            //Get Properties in string
            var i = 0;
            while ((i = str.IndexOf('{', i)) != -1)
            {
                var s = str.Substring(i);
                properties.Add(s.Substring(1, s.IndexOf('}') - 1));
                i++;
            }

            return properties.Aggregate(str, (current, property) => current
                .Replace("{" + property + "}", GetPropertyValue(property, obj)));
        }

        /// <summary>
        /// Determines whether the specified text is nothing.
        /// Use the original method in .Net untill this method does some special
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static bool IsNothing(string text)
        {
            return string.IsNullOrEmpty(text);
        }

        /// <summary>
        /// Convert a given string to statement.
        /// Eg; DateOfBirth => Date of birth
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static string ToStatement(string str)
        {
            str = ToUpperFirst(ToCamelSpaced(str).ToLower());
            return str;
        }

        /// <summary>
        /// To the title.
        /// Eg; ApplicantName => Applicant Name
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static string ToTitle(string str)
        {
            str = ToCamelSpaced(str);
            return TextInfo.ToTitleCase(str);
        }

        /// <summary>
        /// To the upper first.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static string ToUpperFirst(string str)
        {
            return char.ToUpper(str[0]) +
                ((str.Length > 1) ? str.Substring(1).ToLower() : string.Empty);
        }

        /// <summary>
        /// To the camel spaced.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static string ToCamelSpaced(string str)
        {
            return RemovedUnderscore(Regex.Replace(str, "([A-Z])", " $1", RegexOptions.Compiled).Trim());
        }

        /// <summary>
        /// Removes the underscores in a string.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        private static string RemovedUnderscore(string str)
        {
            str = str.Replace('_', ' ');
            return Regex.Replace(str, @"\s+", " ");
        }

        /// <summary>
        /// Gets the property value in an object.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        private static string GetPropertyValue(string property, object obj)
        {
            try
            {
                var pp = property.Split('.');
                var o = obj;
                for (var j = 0; j < pp.Count(); j++)
                {
                    var r = GetProperty(pp[j], o);
                    o = r;
                }
                return o.ToString();
            }
            catch (Exception)
            {
                return "{" + property + "}";
            }

        }

        /// <summary>
        /// Gets the property in a object.
        /// </summary>
        /// <param name="propName">Name of the property.</param>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        private static object GetProperty(string propName, object obj)
        {
            object propValue = null;
            var myType = obj.GetType();
            IList<PropertyInfo> objProps = new List<PropertyInfo>(myType.GetProperties());
            var prop = objProps.SingleOrDefault(p => p.Name == propName);
            if (prop != null) propValue = prop.GetValue(obj, null);
            return propValue;
        }
    }
}
