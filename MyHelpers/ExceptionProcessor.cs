﻿using System;

namespace ALHelpers
{
    public class ExceptionProcessor
    {
        /// <summary>
        /// Gets the errors the message from an exception
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <returns></returns>
        public static string ErrorMessage(Exception ex)
        {
            var msg = ex.Message;
            if (ex.InnerException == null) return msg;
            msg = ex.InnerException.Message;

            if (ex.InnerException.InnerException == null) return msg;
            msg = ex.InnerException.InnerException.Message;

            if (ex.InnerException.InnerException.InnerException != null)
            {
                msg = ex.InnerException.InnerException.InnerException.Message;
            }

            return msg;
        }        
    }
}
