﻿using System;

namespace ALHelpers
{
    public class Date
    {
        /// <summary>
        /// Dates the difference.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        public static string DateDiff(DateTime startDate, DateTime endDate)
        {
            var diff = string.Empty;
            var datediff = (endDate - startDate).Days;
            var yrs = (datediff / 356);
            var mths = ((datediff - (yrs * 356)) / 31);
            var days = ((datediff - (yrs * 356)) - (mths * 31));

            if (yrs > 0) { diff = yrs + " yrs "; }
            if (mths > 0) { diff = diff + mths + " mths "; }
            if (days > 0) { diff = diff + days + " days"; }
            return diff;
        }

        /// <summary>
        /// Times the difference.
        /// </summary>
        /// <param name="starTime">The star time.</param>
        /// <param name="endTime">The end time.</param>
        /// <returns></returns>
        public static string TimeDiff(TimeSpan starTime, TimeSpan endTime)
        {
            var diff = string.Empty;
            var datediff = (endTime - starTime).Ticks;
            var hrs = (datediff / 356);
            var secs = ((datediff - (hrs * 356)) / 31);
            var milsec = ((datediff - (hrs * 356)) - (secs * 31));

            if (hrs > 0) { diff = hrs + " hrs "; }
            if (secs > 0) { diff = diff + secs + " secs "; }
            if (milsec > 0) { diff = diff + milsec + " days"; }
            return diff;
        }
    }
}
