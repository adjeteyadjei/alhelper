﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using String = ALHelpers.String;

namespace Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            var m = new Geek { Name = "Albert", Age = 25, Lang = new Language { Thing = new Thing { Name = ".Net" } } };
            var r = String.Format("My name is {Name} I'm {Age} years old. I have been programming in {Lang.Thing.Name} for {Lang.Duration}.", m);
            var a = ALHelpers.Convertors.Amount.ToEnglish(1000000000.00, "Ghana Cedis", "Peswas");
            
            Console.WriteLine(a);
            Console.Read();
        }
    }

    public class Geek
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Language Lang { get; set; }
    }

    public class Language
    {
        public Thing Thing { get; set; }
        public int Duration { get; set; }
    }

    public class Thing
    {
        public string Name { get; set; }
    }



}
