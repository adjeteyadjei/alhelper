﻿Imports System.Text.RegularExpressions

Public Class TextValidation

    Public Shared Function IsIpValid(ByVal ipAddress As String) As Boolean
        Dim expr As String = "^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$"
        Dim reg As Regex = New Regex(expr)
        If (reg.IsMatch(ipAddress)) Then
            Dim parts() As String = ipAddress.Split(".")
            If Convert.ToInt32(parts(0)) = 0 Then
                Return False
            ElseIf Convert.ToInt32(parts(3)) = 0 Then
                Return False
            End If
            For i As Integer = 1 To 4
                If parts(i) > 255 Then
                    Return False
                End If
            Next
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function IsValidEmail(ByVal email As String) As Boolean
        Dim expr As String = "^([a-zA-Z0-9_\-])([a-zA-Z0-9_\-\.]*)@(\[((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.){3}|" & _
            "((([a-zA-Z0-9\-]+)\.)+))([a-zA-Z]{2,}|(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\])$"
        Dim reg As Regex = New Regex(expr)
        If (reg.IsMatch(email)) = False Then
            Return False
        End If
        Return True
    End Function

End Class
