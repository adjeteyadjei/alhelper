﻿Imports System.Windows.Forms


Public Class FormUtilities

    Public Shared Sub ClearFieldsOnForm(ByVal formControls As Control.ControlCollection)

        For Each ctrl In formControls
            'CLEAR TEXTBOXES, COMBOS AND CHECKBOXES ON A FORM
            If TypeOf ctrl Is TextBox Then ctrl.Text = vbNullString
            If TypeOf ctrl Is RichTextBox Then ctrl.Text = vbNullString
            If TypeOf ctrl Is ComboBox Then ctrl.Text = vbNullString
            If TypeOf ctrl Is CheckBox Then ctrl.Checked = False
            If TypeOf ctrl Is RadioButton Then ctrl.Checked = False
            If TypeOf ctrl Is DateTimePicker Then ctrl.Value = Today.Date
            If TypeOf ctrl Is DataGridView Then ctrl.Rows.Clear()

            'Clear All Check List Boxes
            If TypeOf ctrl Is CheckedListBox Then
                For i As Integer = 0 To ctrl.Items.Count - 1
                    ctrl.SetItemChecked(i, False)
                Next
            End If

            'CLEAR CONTROLS IN A GROUP BOX
            If TypeOf ctrl Is GroupBox Then
                Dim gbos As GroupBox = ctrl
                ClearFieldsOnForm(gbos.Controls)
            End If

            'CLEAR CONTROLS IN A PANEL
            If TypeOf ctrl Is Panel Then
                Dim pnCon As Panel = ctrl
                ClearFieldsOnForm(pnCon.Controls)
            End If

            'CLEAR CONTROLS IN A TABCONTROL
            If TypeOf ctrl Is TabControl Then
                Dim tabCtrl As TabControl = ctrl
                For Each tbcon In tabCtrl.Controls
                    ClearFieldsOnForm(tbcon.Controls)
                Next
            End If
        Next
    End Sub

    Public Shared Function AllowNumberOnly(ByVal ascPress As Integer) As Boolean
        If ascPress > 47 And ascPress < 58 Or ascPress = 8 Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Shared Function FloatingNumbers(ByVal ascPress As Integer) As Boolean
        If ascPress > 47 And ascPress < 58 Or ascPress = 8 Or ascPress = 46 Then
            Return False
        Else
            Return True
        End If
    End Function


End Class
