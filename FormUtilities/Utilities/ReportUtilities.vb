﻿Imports System.Windows.Forms
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Office.Interop

Public Class ReportUtilities

    Private Shared xlApp As Object
    Private Shared xlWb As Object
    Private Shared xlWs As Object

    Public Shared Sub ExportDataToExcel(ByVal DataGridViewControl As DataGridView)

        If DataGridViewControl.RowCount = 0 Then Exit Sub

        Dim dgColumnCount As Integer = DataGridViewControl.ColumnCount

        CreateInstanceAndSetControls()

        'Copy field names to the first row of the worksheet
        For iCol As Integer = 1 To dgColumnCount
            xlWs.Cells(1, iCol).Value = DataGridViewControl.Columns(iCol - 1).Name
        Next

        'This only works for 2003 to 2007 excel files
        If Val(Mid(xlApp.Version, 1, InStr(1, xlApp.Version, ".") - 1)) > 8 Then
            ' Copy the recordset to the worksheet, starting in cell A2
            For x As Integer = 0 To DataGridViewControl.RowCount - 1
                For y As Integer = 0 To DataGridViewControl.ColumnCount - 1
                    xlWs.Cells(x + 2, y + 1).Value = DataGridViewControl.Rows(x).Cells(y).Value
                Next
            Next
        End If

        AutoFitAndClose()

    End Sub

    Public Shared Sub ExportDataToExcel(myDataTable As DataTable)

        If myDataTable.Rows.Count = 0 Then Exit Sub

        CreateInstanceAndSetControls()

        'Copy field names to the first row of the worksheet
        For iCol As Integer = 0 To myDataTable.Columns.Count - 1
            xlWs.Cells(1, iCol + 1).Value = myDataTable.Columns(iCol).ColumnName
        Next

        'This only works for 2003 to 2007 excel files
        If Val(Mid(xlApp.Version, 1, InStr(1, xlApp.Version, ".") - 1)) > 8 Then
            ' Copy the datatable to the worksheet, starting in cell A2
            For x As Integer = 0 To myDataTable.Rows.Count - 1
                For y As Integer = 0 To myDataTable.Columns.Count - 1
                    xlWs.Cells(x + 2, y + 1).Value = myDataTable.Rows(x)(y)
                Next
            Next
        End If

        AutoFitAndClose()

    End Sub

    Private Shared Sub AutoFitAndClose()

        'Auto-fit the column widths and row heights
        xlApp.Selection.CurrentRegion.Columns.AutoFit()
        xlApp.Selection.CurrentRegion.Rows.AutoFit()


        'Release Excel references
        xlWs = Nothing
        xlWb = Nothing
        xlApp = Nothing

    End Sub

    Private Shared Sub CreateInstanceAndSetControls()

        'Create an Instance of Excel
        xlApp = CreateObject("Excel.Application")
        xlWb = xlApp.Workbooks.Add
        xlWs = xlWb.Worksheets("Sheet1")

        'Display Excel and give user control of Excel's lifetime
        xlApp.Visible = True
        xlApp.UserControl = True

    End Sub

    Public Shared Sub InvoiceReceiptExcel(customer As String, invoiceList As List(Of InvoiceTemp))
        Dim objExcel As New Excel.Application
        Dim objWorkbook As Excel.Workbook, objSheet As Excel.Worksheet
        Dim strFileName As String

        Try
            strFileName = My.Application.Info.DirectoryPath & "\InvoiceTemplate.xlsx"
            objWorkbook = objExcel.Workbooks.Add(strFileName)
            objSheet = objWorkbook.Sheets("Receipt")

            'Insert Date Values
            objSheet.Cells(3, 2) = Today.Date

            'Insert Customer
            objSheet.Cells(4, 3) = customer

            'Insert Invoice
            Dim r As Integer = 6, totalCost As Double = 0.0
            For Each item In invoiceList
                objSheet.Cells(r, 1) = item.Quantity
                objSheet.Cells(r, 2) = item.Item
                objSheet.Cells(r, 3) = item.Unit
                objSheet.Cells(r, 4) = item.Price
                r = r + 1
                totalCost = totalCost + item.Price
            Next

            'Total Amount
            objSheet.Cells(r, 2) = "TOTAL AMOUNT:"
            objSheet.Cells(r, 4) = totalCost

            'Show Excel Workbook
            objExcel.Visible = True
        Catch ex As Exception
            objExcel.Quit()
        End Try

    End Sub

    Public Class InvoiceTemp
        Private _Quantity As Int32
        Private _Item As String
        Private _Unit As Double
        Private _Price As Double

        Public Property Price() As Double
            Get
                Return _Price
            End Get
            Set(ByVal value As Double)
                _Price = value
            End Set
        End Property

        Public Property Unit() As Double
            Get
                Return _Unit
            End Get
            Set(ByVal value As Double)
                _Unit = value
            End Set
        End Property

        Public Property Item() As String
            Get
                Return _Item
            End Get
            Set(ByVal value As String)
                _Item = value
            End Set
        End Property

        Public Property Quantity() As Int32
            Get
                Return _Quantity
            End Get
            Set(ByVal value As Int32)
                _Quantity = value
            End Set
        End Property

    End Class


End Class
